package br.ucsal.bes20201.testequalidade.veiculosbuilder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {
	
	private static final String PLACA_DEFAULT = "ABC1234";
	private static final Integer ANO_DEFAULT = 2030;
	private static final Modelo MODELO_DEFAULT = new Modelo("KA");
	private static final Double VALORDIARIA_DEFAULT = 100.00;
	private static final SituacaoVeiculoEnum SITUACAO_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL;
	
	private String placa = PLACA_DEFAULT;
	private Integer ano = ANO_DEFAULT;
	private	Modelo modelo = MODELO_DEFAULT;
	private Double valorDiaria = VALORDIARIA_DEFAULT;
	private SituacaoVeiculoEnum situacao = SITUACAO_DEFAULT;
	
	
	private VeiculoBuilder() {}
	
	private static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}
	
	public static VeiculoBuilder umVeiculoBuilder() {
		return new VeiculoBuilder();
	}
	
	public VeiculoBuilder ComPlaca(String placa) {
		this.placa = placa;
		return this;
	}
	
	public VeiculoBuilder comAno(Integer ano) {
		this.ano = ano;
		return this;
	}
	
	public VeiculoBuilder comModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}
	
	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}
	
	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	
	private static VeiculoBuilder umVeiculoDisponivelBuilder() {
		return umVeiculoBuilder().disponivel();
	}
	
	private static VeiculoBuilder umVeiculoLocadoBuilder() {
		return umVeiculoBuilder().disponivel();
	}
	
	private static VeiculoBuilder umVeiculoManutencaoBuilder() {
		return umVeiculoBuilder().disponivel();
	}
	
	public VeiculoBuilder disponivel() {
		this.situacao = SituacaoVeiculoEnum.DISPONIVEL;
		return this;
	}

	public VeiculoBuilder locado() {
		this.situacao = SituacaoVeiculoEnum.LOCADO;
		return this;
	}
	
	public VeiculoBuilder manutencao() {
		this.situacao = SituacaoVeiculoEnum.MANUTENCAO;
		return this;
	}

	public VeiculoBuilder mas() {
		return umVeiculoBuilder().ComPlaca(placa).comAno(ano).comModelo(modelo).comValorDiaria(valorDiaria).comSituacao(situacao);
	}

	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setAno(ano);
		veiculo.setModelo(modelo);
		veiculo.setPlaca(placa);
		veiculo.setSituacao(situacao);
		veiculo.setValorDiaria(valorDiaria);
		return veiculo;
	}
	
}
