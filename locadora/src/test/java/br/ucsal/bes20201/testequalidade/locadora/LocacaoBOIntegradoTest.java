package br.ucsal.bes20201.testequalidade.locadora;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.persistence.VeiculoDAO;
import br.ucsal.bes20201.testequalidade.veiculosbuilder.VeiculoBuilder;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculoBuilder().comAno(2012);
		Veiculo v1 = veiculoBuilder.build(); 
		Veiculo v2 = veiculoBuilder.build();
		Veiculo v3 = veiculoBuilder.build(); 
		veiculoBuilder = VeiculoBuilder.umVeiculoBuilder().comAno(2019);
		Veiculo v4 = veiculoBuilder.build();
		Veiculo v5 = veiculoBuilder.build();
		List<Veiculo> veiculos = Arrays.asList(v1,v2,v3,v4,v5);
		
		
		VeiculoDAO.insert(v1);
		VeiculoDAO.insert(v2);
		VeiculoDAO.insert(v3);
		VeiculoDAO.insert(v4);
		VeiculoDAO.insert(v5);
		
		Double valorAtual = LocacaoBO.calcularValorTotalLocacao(veiculos, 3);
		Double valorEsperado = 1410.0;
		assertEquals(valorEsperado, valorAtual);
		
	}

}
